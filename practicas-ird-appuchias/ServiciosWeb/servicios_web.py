#! /usr/bin/env python3

from argparse import ArgumentParser, Namespace
from time import sleep
import bs4, requests

from gmg import gmg

try:
    with open("token", "r") as f:
        TOKEN = f.read().strip()
except FileNotFoundError:
    print("No se ha encontrado el archivo `token`.")
    print("Por favor, crea un archivo llamado `token` con el token de LocationIQ.")
    exit(1)

URL_AYTOS = "https://irdgcdinfo.data.blog/ayuntamientos/"
URL_CODIGOS = "https://irdgcdinfo.data.blog/codigos/"
BASE_URL_LOC = f"https://eu1.locationiq.com/v1/search?key={TOKEN}&q="
BASE_URL_METEOGAL = (
    "http://servizos.meteogalicia.gal/rss/predicion/jsonPredConcellos.action?idConc="
)
DAY_SEGMENTS = ["manha", "tarde", "noite"]
DAY_SEGMENT = DAY_SEGMENTS[1]


def get_aytos(url: str = URL_AYTOS) -> dict[str, int]:
    """Get aytos from URL_AYTOS"""

    aytos = {}
    r = requests.get(url)
    assert r.status_code == 200, r.status_code
    soup = bs4.BeautifulSoup(r.text, "html.parser")

    # Find all rows
    rows = soup.find_all("tr")
    for row in rows:
        cols = row.find_all("th")
        if cols[0].text == "Identificador":
            continue

        aytos[cols[1].text] = int(cols[0].text)

    return aytos


def get_codigos(url: str = URL_CODIGOS) -> dict[int, str]:
    """Get sky state codes from URL_CODIGOS"""

    codigos = {}
    r = requests.get(url)
    assert r.status_code == 200, r.status_code
    soup = bs4.BeautifulSoup(r.text, "html.parser")

    # Find all rows
    rows = soup.find_all("tr")
    for row in rows:
        cols = row.find_all("th")
        if cols[0].text == "Valor numérico":
            continue

        codigos[int(cols[0].text)] = cols[1].text

    return codigos


def get_meteogal(id_mun: int, url: str = BASE_URL_METEOGAL) -> dict:
    """Get meteogal data from URL_METEOGAL"""

    url += str(id_mun)
    r = requests.get(url)

    assert r.status_code == 200, r.status_code
    data = r.json()

    return data


def get_location(ayto: str, url: str = BASE_URL_LOC) -> tuple[float, float]:
    """Get location from BASE_URL_LOC"""

    dict_places = {}
    r = requests.get(url + ayto + "&format=xml")

    request_count = 0
    while r.status_code == 429 and request_count < 10:
        sleep(0.5)
        r = requests.get(url + ayto + "&format=xml")
        request_count += 1

    if request_count == 10:
        print(f"Error: demasiadas peticiones [{ayto}]")
        return 0.0, 0.0

    assert r.status_code == 200, r.status_code
    soup = bs4.BeautifulSoup(r.text, "xml")

    # Find location
    places = soup.find_all("place")
    for place in places:
        dict_places[place["importance"]] = {
            "lat": float(place["lat"]),
            "lon": float(place["lon"]),
        }

    max_importance = max(dict_places)

    return dict_places[max_importance]["lat"], dict_places[max_importance]["lon"]


def get_args() -> Namespace:
    """Parse arguments from command line"""

    parser = ArgumentParser()
    parser.add_argument(
        "aytos",
        help="Ayuntamiento[s] separados por comas. \
            Si no se especifica ninguno, se mostrará la lista de ayuntamientos disponibles",
        type=str,
        nargs="*",
    )
    parser.add_argument(
        "-s",
        "--segmento",
        help=f"Segmento del día a mostrar. Por defecto: {DAY_SEGMENT}",
        type=str,
        default=DAY_SEGMENT,
        choices=DAY_SEGMENTS,
    )

    return parser.parse_args()


def main():
    # Get aytos and codes
    aytos_ids = get_aytos()
    ids_skycodes = get_codigos()

    # Get municipality ID
    args = get_args()
    DAY_SEGMENT = args.segmento
    if not args.aytos:
        print("Ayuntamientos disponibles:")
        for ayto in sorted(aytos_ids):
            print(f" - {ayto}")
        print(
            "En caso de que el nombre del ayuntamiento contenga espacios, se debe escribir entre comillas"
        )
        return

    # Get weather data
    aytos = [ayto.strip() for ayto in args.aytos[0].split(",")]

    not_found = [ayto for ayto in aytos if ayto not in aytos_ids]

    if any(not_found):
        print(f"Ayuntamiento[s] no encontrado[s]: {not_found}")
        return

    weather_data = {
        ayto: get_meteogal(aytos_ids[ayto])["predConcello"]["listaPredDiaConcello"][0]
        for ayto in aytos
    }

    points = []

    for mun, data in weather_data.items():
        sky_code = data["ceo"][DAY_SEGMENT]

        lat, lon = get_location(mun)

        # Print weather data
        print(f"Predicción para hoy en {mun}")
        print(f"Lat/lon: {lat} / {lon}")
        print(f"Temperatura máx/mín: {data['tMax']} / {data['tMin']} ºC")
        print(f"Índice UV: {data['uvMax']}")
        print(f"Estado del cielo: {ids_skycodes[sky_code]} ({sky_code})")
        print(f"Probabilidad de lluvia: {data['pchoiva'][DAY_SEGMENT]}%")
        print("-" * 40 + "\n")

        # Add point to plot
        points.append((sky_code, (lon, lat)))

    print("Generando mapa...")
    print("-----------------")
    gmg.plotMap(points)


if __name__ == "__main__":
    # Map is using "A Coruña, Lugo, Ourense, Pontevedra, Vigo, Fisterra, Noia, Entrimo"
    main()
