""" Servidor UDP """

import sys
import socket


def main():

    if len(sys.argv) != 2:
        print("Formato ServidorUDP <puerto>")
        sys.exit()

    try:
        # Instrucciones de sockets

        # Leemos los argumentos necesarios
        puerto = int(sys.argv[1])

        # Creamos el socket no orientado a conexión
        socketServidor = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # Asociamos el socket a cualquier dirección local en el puerto indicado
        socketServidor.bind(("", puerto))

        # Establecemos un timeout de 300 segundos
        timeout = 300
        socketServidor.settimeout(timeout)

        # Mostramos por pantalla que el servidor ya está escuchando
        print("Iniciado servidor en PUERTO: {}".format(puerto))

        while True:
            # Recibimos el mensaje
            mensaje, direccion = socketServidor.recvfrom(4096)

            print(
                "SERVIDOR: Recibido {} de {}:{}".format(
                    mensaje.decode("UTF-8"), direccion[0], direccion[1]
                )
            )

            # Reenviamos el mensaje
            socketServidor.sendto(mensaje, direccion)

    except socket.timeout:
        # Captura excepción si el tiempo de espera se agota
        print("{} segundos sin recibir nada.".format(timeout))
    except:
        # Captura excepción genérica.
        print("Error: {}".format(sys.exc_info()[0]))
        raise
    finally:
        # En cualquier caso cierra el socket
        socketServidor.close()


if __name__ == "__main__":
    main()
