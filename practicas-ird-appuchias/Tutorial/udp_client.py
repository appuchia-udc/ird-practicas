import sys, socket

from argparse import ArgumentParser


def get_args():
    parser = ArgumentParser(description="UDP datagram client")
    parser.add_argument(
        "-d",
        metavar="HOST",
        help="send to HOST",
        required=True,
    )
    parser.add_argument(
        "-p",
        metavar="PORT",
        type=int,
        default=9999,
        help="UDP port (default 9999)",
        required=True,
    )
    parser.add_argument(
        "-m",
        metavar="MESSAGE",
        default="Hello UDP!",
        help="message to send (default 'Hello UDP!')",
    )

    return parser.parse_args()


def send_datagram(host, port, message):
    timeout = 60
    socketCliente = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    socketCliente.settimeout(timeout)

    try:
        print(f"CLIENTE: Enviando {message} a {host}:{port}")
        # Enviamos el mensaje a la máquina y puerto indicados
        socketCliente.sendto(message.encode("UTF-8"), (host, port))

        # Recibimos el mensaje de respuesta
        mensajeEco, a = socketCliente.recvfrom(len(message))
        print(f"CLIENTE: Recibido {mensajeEco.decode('UTF-8')} de {host}:{port}")

    except socket.timeout:
        # Captura excepción si el tiempo de espera se agota.
        print("{} segundos sin recibir nada.".format(timeout))
    except:
        # Captura excepción genérica.
        print("Error: {}".format(sys.exc_info()[0]))
        raise
    finally:
        # En cualquier caso cierra el socket.
        socketCliente.close()


if __name__ == "__main__":
    args = get_args()
    send_datagram(args.d, args.p, args.m)
