import sys, socket

from argparse import ArgumentParser


def get_args():
    parser = ArgumentParser(description="UDP datagram server")
    # parser.add_argument(
    #     "-h",
    #     metavar="HOST",
    #     help="send to HOST",
    # )
    parser.add_argument(
        "-p",
        metavar="PORT",
        type=int,
        default=9999,
        help="UDP port (default 9999)",
        required=True,
    )

    return parser.parse_args()


def recv_datagram(port):
    timeout = 60
    socketCliente = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    socketCliente.settimeout(timeout)

    try:
        # Creamos el socket no orientado a conexión
        socketServidor = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # Asociamos el socket a cualquier dirección local en el puerto indicado
        socketServidor.bind(("", port))

        print("Iniciando servidor en PUERTO: ", port)

        while True:
            # Recibimos el mensaje
            mensaje, direccion = socketServidor.recvfrom(4096)
            print(f"{direccion[0]}:{direccion[1]} -> {mensaje.decode('UTF-8')}")

            # Enviamos el mensaje de vuelta
            socketServidor.sendto(mensaje, direccion)

    except socket.timeout:
        # Captura excepción si el tiempo de espera se agota.
        print("{} segundos sin recibir nada.".format(timeout))
    except:
        # Captura excepción genérica.
        print("Error: {}".format(sys.exc_info()[0]))
        raise
    finally:
        # En cualquier caso cierra el socket.
        socketCliente.close()


if __name__ == "__main__":
    port = get_args().p
    print(f"Escuchando en el puerto {port}...")
    recv_datagram(port)
