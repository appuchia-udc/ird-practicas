#! /usr/bin/env python3

import socket
from argparse import ArgumentParser


TIMEOUT = 300


def recv_tcp(address: str, port: int):
    """Listens for messages in `address`:`port` using TCP."""
    with socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM) as server:
        server.settimeout(TIMEOUT)

        # Bind to the desired port
        server.bind((address, port))
        server.listen()
        print(f"Server listening on port :{port}")

        while True:
            try:
                # Wait for connection
                conn, remote = server.accept()
                remote_host, remote_port = remote
                print(f"Connection established with host {remote_host}:{remote_port}")

                # Receive message
                msg = conn.recv(4096).decode("UTF-8")
                print(f"Message received from {remote_host}:{remote_port}: {msg}")

                # Reply and close the connection
                conn.send(msg.encode("UTF-8"))
                conn.close()

            except socket.timeout:
                print(f"{TIMEOUT}s timeout was exceeded without response.")
            except Exception as e:
                print("An error ocurred. Closing the connection.")
                raise e


def get_args():
    """Returns the arguments passed to the script."""
    parser = ArgumentParser()
    parser.add_argument(
        "-a",
        "--address",
        default="localhost",
        help="Address to listen on",
        required=False,
    )
    parser.add_argument(
        "-p",
        "--port",
        default="9999",
        help="Port to listen on.",
        type=int,
    )

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = get_args()
    address, port = args.address, args.port

    recv_tcp(address, port)
