#! /usr/bin/env python3

import socket
from argparse import ArgumentParser
from threading import Thread


TIMEOUT = 300


def manage_client(client_socket, remote):
    """Manages a client connection.
    Receives a message and replies with the same message.
    """
    remote_host, remote_port = remote

    # Receive message
    msg = client_socket.recv(4096).decode("UTF-8")
    print(f"Message received from {remote_host}:{remote_port}: {msg}")

    # Reply and close the connection
    client_socket.send(msg.encode("UTF-8"))
    client_socket.close()


def recv_tcp(address: str, port: int):
    """Listens for messages in `address`:`port` using TCP.

    It will create a new thread for each client connection,
    so it can handle multiple clients at the same time.
    """
    with socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM) as server:
        server.settimeout(TIMEOUT)

        # Bind to the desired port
        server.bind((address, port))
        server.listen()
        print(f"Server listening on port :{port}")

        while True:
            try:
                # Wait for connection
                conn, remote = server.accept()
                remote_host, remote_port = remote
                print(f"Connection established with host {remote_host}:{remote_port}")

                # Process request
                Thread(target=manage_client, args=(conn, remote)).start()

            except socket.timeout:
                print(f"{TIMEOUT}s timeout was exceeded without response.")
            except Exception as e:
                print("An error ocurred. Closing the connection.")
                raise e


def get_args():
    """Returns the arguments passed to the script."""
    parser = ArgumentParser()
    parser.add_argument(
        "-a",
        "--address",
        default="localhost",
        help="Address to listen on",
        required=False,
    )
    parser.add_argument(
        "-p",
        "--port",
        default="9999",
        help="Port to listen on.",
        type=int,
    )

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = get_args()
    address, port = args.address, args.port

    recv_tcp(address, port)
