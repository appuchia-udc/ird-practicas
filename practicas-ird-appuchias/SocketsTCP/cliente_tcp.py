#! /usr/bin/env python3

import socket
from argparse import ArgumentParser


TIMEOUT = 300


def send_tcp(host: str, port: int, msg: str) -> None:
    """Sends `msg` to `host`:`port` using TCP."""
    with socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM) as client:
        client.settimeout(TIMEOUT)
        try:
            print(f"Sending `{msg}` to {host}:{port}")

            client.connect((host, port))
            client.send(msg.encode("UTF-8"))

            response = client.recv(4096).decode("UTF-8")

            print(f"Received response {response} from {host}:{port}")

        except socket.timeout:
            print(f"{TIMEOUT}s timeout was exceeded without response.")
        except Exception as e:
            print("An error ocurred. Closing the connection.")
            raise e


def get_args():
    """Returns the arguments passed to the script."""
    parser = ArgumentParser()
    parser.add_argument(
        "destination",
        help="Machine you want to connect to.",
    )
    parser.add_argument(
        "port",
        help="Port used for the connection.",
        type=int,
    )
    parser.add_argument(
        "-m",
        "--message",
        default="Hello, world!",
        help="Message to send to destination.",
    )

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = get_args()
    host, port, message = args.destination, args.port, args.message

    send_tcp(host, port, message)
