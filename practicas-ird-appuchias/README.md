# Prácticas IRD

[![GPL 3.0 License](https://img.shields.io/badge/License-GPL%20v3.0-brightgreen?style=flat-square)](https://github.com/GCED-IRD-614G020102223/practicas-ird-appuchias/blob/master/LICENSE)
[![Author](https://img.shields.io/badge/Project%20by-Appu-9cf?style=flat-square)](https://github.com/appuchias)

## Qué es esto?

Este proyecto contiene el código presentado a las prácticas de la asignatura de Internet: Redes y Datos.

## License

This project is licensed under the [GPL 3.0 License](https://github.com/GCED-IRD-614G020102223/practicas-ird-appuchias/blob/master/LICENSE).

Built with 🖤 by Appu
