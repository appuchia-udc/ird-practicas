#! /usr/bin/env python3

# Servidor web multihilo

import socket
from argparse import ArgumentParser
from threading import Thread

from handler import reply, set_verbosity


TIMEOUT = 300
BREAK_ON_TIMEOUT = False
DEFAULT_ADDRESS = "0.0.0.0"
DEFAULT_PORT = 8000


def run_server(address: str, port: int):
    """Starts a web server listening on `address`:`port`.

    The server will listen for connections and will reply with the same message
    it receives.
    """

    with socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM) as server:
        server.settimeout(TIMEOUT)

        # Bind to the desired port
        server.bind((address, port))
        server.listen()
        print(f"Server listening on {address}:{port}", end="\n\n")

        while True:
            try:
                # Wait for connection
                conn, remote = server.accept()
                remote_host, remote_port = remote
                if VERBOSE > 2:
                    print(f"Connection established with {remote_host}:{remote_port}")

                # Reply to the client
                Thread(target=reply, args=(conn, remote_host, int(remote_port))).start()

            except socket.timeout:
                if BREAK_ON_TIMEOUT:
                    print(f"{TIMEOUT}s timeout was exceeded without response.")
                    break
            except Exception as e:
                print("An error ocurred. Closing the connection.")
                raise e


def get_args():
    """Returns the arguments passed to the script."""
    parser = ArgumentParser()
    parser.add_argument(
        "-a",
        "--address",
        default=DEFAULT_ADDRESS,
        help=f"Address to listen on. (Default: {DEFAULT_ADDRESS})",
        required=False,
    )
    parser.add_argument(
        "-p",
        "--port",
        default=DEFAULT_PORT,
        help=f"Port to listen on. (Default: {DEFAULT_PORT})",
        type=int,
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        help="Prints more information. (-v, -vv, -vvv)",
    )
    parser.add_argument(
        "-t",
        "--timeout",
        default=TIMEOUT,
        help="Timeout in seconds. (Default: 300)",
        type=int,
    )
    parser.add_argument(
        "-b",
        "--break-on-timeout",
        action="store_true",
        help="Stop server if no connection is received until timeout. (Default: False)",
    )

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = get_args()
    VERBOSE = args.verbose
    TIMEOUT = args.timeout
    BREAK_ON_TIMEOUT = args.break_on_timeout
    set_verbosity(VERBOSE)
    address, port = args.address, args.port

    run_server(address, port)
