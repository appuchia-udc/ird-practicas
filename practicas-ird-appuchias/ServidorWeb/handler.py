#! /usr/bin/env python3

import os
from dataclasses import dataclass
from datetime import datetime

ALLOWED_METHODS = ["GET", "HEAD"]
CONTENT_TYPES = {
    "html": "text/html",
    "txt": "text/plain",
    "jpg": "image/jpeg",
    "jpeg": "image/jpeg",
    "gif": "image/gif",
}
SERVER_NAME = "AppuServer/1.0"
STATUS_CODES = {
    200: "OK",
    301: "Moved Permanently",
    400: "Bad Request",
    404: "Not Found",
    405: "Method Not Allowed",
    500: "Internal Server Error",
    501: "Not Implemented",
}
VERBOSITY = 0


def set_verbosity(verbose: int):
    """Sets the verbosity of the module."""

    global VERBOSITY
    VERBOSITY = verbose


# Even though the module `requests` exists, I only need this implementation
@dataclass
class Request:
    """A class to represent a request."""

    method: str
    path: str
    version: str
    headers: dict[str, str]
    host: str
    port: int

    def __str__(self) -> str:
        """Returns a string representation of the request."""

        return f"{self.host}:{self.port} {self.method:<5} {self.path:<30}"


def reply(client_socket, remote_host: str, remote_port: int) -> None:
    """Processes a request from a client and sends a response."""

    # Receive message
    raw_request = client_socket.recv(4096).decode("UTF-8")
    request = parse_request(raw_request, remote_host, remote_port)

    if request.method not in ALLOWED_METHODS:
        if VERBOSITY > 0:
            print(request, end="")

        response = raise_error(400)
        client_socket.send(response)
        client_socket.close()
        return

    if VERBOSITY > 0:
        print(request, end="")

    response = process_request(request)

    # Reply and close the connection
    client_socket.send(response)
    client_socket.close()


def parse_request(raw_request: str, host: str, port: int) -> Request:
    """Parses the request and returns it as a Request object."""

    try:
        petition_line, *headers = raw_request.splitlines()
        method, path, version = petition_line.split()
    except ValueError:
        method, path, version = "ERROR", "Method not supported", "HTTP/1.0"
        headers = []

    request = Request(method, path, version, dict(), host, port)

    for header in headers:
        # To get away with the colon in the value of Host header
        key, value = header.split(":")[0], ":".join(header.split(":")[1:])
        request.headers[key] = value

    return request


def process_request(request: Request) -> bytes:
    """Processes a request and returns the response."""

    # Redirect to index.html if the path is /
    if request.path == "/":
        return craft_response(301, {"Location": "index.html"}, "")

    # Return only the headers if the method is HEAD
    head = request.method == "HEAD"

    # Check if the file exists
    file_path = "data/" + request.path[1:]
    if not os.path.isfile(file_path):
        return raise_error(404)

    # Set MIME type
    extension = request.path.split(".")[-1]
    if extension not in CONTENT_TYPES:
        mime = "application/octet-stream"
    mime = CONTENT_TYPES[extension]

    # Read file (text or binary)
    if mime.split("/")[0] == "text":
        with open(file_path, "r") as f:
            message = f.read()
    else:
        with open(file_path, "rb") as f:
            message = f.read()

    # Create response headers
    response_headers = {
        "Content-Type": mime,
        "Content-Length": str(os.path.getsize(file_path)),
        "Last-Modified": datetime.fromtimestamp(os.path.getmtime(file_path)).strftime(
            "%a, %d %b %Y %H:%M:%S %Z"
        ),
    }

    return craft_response(200, response_headers, message if not head else "")


def raise_error(status: int) -> bytes:
    """Processes an error and returns the response."""

    message = f"<h1>Error {status}</h1><p>{STATUS_CODES[status]}</p>"

    return craft_response(
        status,
        {
            "Content-Type": "text/html",
            "Content-Length": str(len(message)),
        },
        message,
    )


def craft_response(status: int, headers: dict[str, str], body: str | bytes) -> bytes:
    """Creates a response from a status code and a body."""

    # Add default headers
    headers["Connection"] = "close"
    headers["Date"] = datetime.now().strftime("%a, %d %b %Y %H:%M:%S %Z")
    headers["Server"] = SERVER_NAME

    # Add status line and headers to response
    response = f"HTTP/1.0 {status} {STATUS_CODES[status]}\r\n"
    response += "\r\n".join([f"{key}: {value}" for key, value in headers.items()])

    # Add an empty line to separate headers from body
    response += "\r\n\r\n"

    # Add body to response. Body can be a string if it is a text file or bytes if it is a binary file.
    if isinstance(body, str):
        response += body
        response = response.encode("UTF-8")
    else:
        response = response.encode("UTF-8") + body

    if VERBOSITY > 0:
        print(f" -> {status} {STATUS_CODES[status]} ({len(body)} B)")

    return response
