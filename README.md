# IRD

[![Author](https://img.shields.io/badge/Project%20by-Appu-9cf?style=flat-square)](https://gitlab.com/appuchia)

## Qué es esto

Aquí está el código que he escrito en las clases de Internet: Redes y datos.

## Setup

1. Navigate to the desired folder: `cd <path>`
1. Clone the repo: `git clone https://gitlab.com/appuchia/appuchia-udc/ird-practicas.git`
1. Navigate into the repo folder: `cd ird-practicas`
1. Install the project dependencies: `pip install -r requirements.txt`
1. Run the file: `python3 <archivo.py>`

## Licencia

El código está licenciado bajo la [licencia GPLv3](https://gitlab.com/appuchia/appuchia-udc/ird-practicas/-/blob/master/LICENSE).

Coded with 🖤 by Appu
